#! /bin/awk -f
#this script checks the R3B ASCII events from a generator for obvious inconsistencies.
BEGIN {
	nb_events = 0; nb_tracks = 0;
        nb_tracks_hist[0] = 0;
        max_trk = 0; min_trk = 65536;
}
NF == 4 {
	    nb_events++;
            nb_tracks += $2;
            if( $2 in nb_tracks_hist ) nb_tracks_hist[$2] += 1;
            else nb_tracks_hist[$2] = 1;
            if( $2 > max_trk ){ max_trk = $2 }
            if( $2 < min_trk ){ min_trk = $2 }
	}
END {
	print "Events read:", nb_events;
        print "Tracks read:", nb_tracks;
        print "Histogram for #TRACKS follows:\n";
        for( ii = min_trk; ii <= max_trk; ++ii ){
            if( ii in nb_tracks_hist ) print ii, nb_tracks_hist[ii];
            else print ii, 0;
        }
}
