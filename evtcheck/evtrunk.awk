#! /bin/awk -f

BEGIN{
    nb_events = 0;
    nb_print = 10;
    for( i=0; i < ARGC-1; ++i ){
        if( ARGV[i] == "+n" ) nb_print = ARGV[i+1];
    }
}
{
    if( NF == 4 ){
        nb_events++;
        if( nb_events > nb_print ) exit( 0 );
        print $0;
    } else if( NF == 10 ) { print $0; }
}
