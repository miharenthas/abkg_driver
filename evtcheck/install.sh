#! /bin/bash

if ! [ -e /usr/local/bin/evtcheck.awk ]; then
	ln -s $PWD/evtcheck.awk /usr/local/bin/
fi

if ! [ -e /usr/local/bin/evtrunk.awk ]; then
	ln -s $PWD/evtrunk.awk /usr/local/bin/
fi

if ! [ -e /usr/local/bin/evtstats.awk ]; then
        ln -s $PWD/evtstats.awk /usr/local/bin/
fi
