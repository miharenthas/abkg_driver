#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class R3BTarget+;
#pragma link C++ class R3BTargetWheel+;
#pragma link C++ class R3BTargetAtmosphere+;
#pragma link C++ class R3BTargetShielding+;
#pragma link C++ class R3BMagnet+;
#pragma link C++ class R3BTargetAssemblyMedia+;

#endif
