/* This tiny program is intended to convert 2d histograms
 * into Octave ascii worksapces, compatible with their
 * 2d histograms, or my gnuplot functions.
 */

#include <stdio.h>
#include <getopt.h>
#include <string.h>
#include <vector>

/* root junk */
#include "TH2.h"
#include "TFile.h"
#include "TKey.h"

/* old fashioned exit codes */
#define FILE_ERROR 1

/* old fashioned flags */
#define TO_FILE  0x1
#define VERBOSE 0x2

void dump_histo( FILE *stream, TH2F *h, const char *name );

int main( int argc, char **argv ){
    char infname[256];
    char outfname[256];
    int flags = 0;

    struct option opts[] = {
        { "output-file", required_argument, NULL, 'o' },
        { "verbose", no_argument, &flags, flags | VERBOSE },
        { 0, 0, 0, 999 }
    };

    for( int i=1; i < argc; ++i ){
        if( argv[i][0] == '-' ) break;
        strncpy( infname, argv[i], 256 );
    }

    char iota = 0; int opt_idx = 0;
    while( (iota = getopt_long( argc, argv, "o:v", opts, &opt_idx )) != -1 ){
        switch( iota ){
            case 'o':
                flags |= TO_FILE;
                strncpy( outfname, optarg, 256 );
                break;
            case 'v':
                flags |= VERBOSE;
                break;
            default:
                fprintf( stderr, "%s: option %c not recognised.\n", argv[0], iota );
                break;
        }
    }

    if( flags & VERBOSE ){
        puts( "*** Welcome in the root histo to octave dumper! ***" );
        printf( "Reading from file: %s\n", infname );
    }
    TFile f( infname );
    if( f.IsZombie() ){
        if( flags & VERBOSE ) fprintf( stderr, "File probbblemmm...\n" );
        exit( FILE_ERROR );
    }


    char *histo_names[256];
    TIter nextkey( f.GetListOfKeys() );
    TKey *key;
    TObject *obj;
    TH2F *histos[256];
    int nb_histos = 0;
    while( (key = (TKey*)nextkey()) != NULL && nb_histos < 256 ){
        obj = key->ReadObj();
        if( obj->IsA()->InheritsFrom( TH2F  ::Class() ) ){
            histos[nb_histos] = (TH2F*)obj;
            histo_names[nb_histos] = (char*)calloc( 256, 1 );
            strncpy( histo_names[nb_histos], histos[nb_histos]->GetName(), 256 );
            if( flags & VERBOSE ) printf( "Found histo: %s\n", histo_names[nb_histos] );
            nb_histos++;
        }
    }

    FILE *ofile = (flags & TO_FILE)? fopen( outfname, "w" ) : stdout;

    for( int hh=0; hh < nb_histos; ++hh ){
        if( flags & VERBOSE ) printf( "Putting histo %s\n", histo_names[hh] );
        dump_histo( ofile, histos[hh], histo_names[hh] );
    }

    fflush( ofile );
    fclose( ofile );
    f.Close();
    for( int ii=0; ii < nb_histos; ++ii ) free( histo_names[ii] );

    if( flags & VERBOSE ) puts( "*** Done, goodbye. ***" );
    return 0;
}

void dump_histo( FILE *stream, TH2F *h, const char *name ){
    int nbx = h->GetNbinsX();
    int nby = h->GetNbinsY();
    #define mati( i, j ) (j)*nbx + i

    double *binx, *biny, *bigmatrix;
    binx = (double*)malloc( nbx*sizeof( double ) );
    biny = (double*)malloc( nby*sizeof( double ) );
    bigmatrix = (double*)malloc( nbx*nby*sizeof( double ) );

    for( int ii=0; ii < nbx; ++ii ) binx[ii] = h->GetXaxis()->GetBinCenter( ii+1 );
    for( int jj=0; jj < nby; ++jj ) biny[jj] = h->GetYaxis()->GetBinCenter( jj+1 );
    
    for( int ii=0; ii < nbx; ++ii ) for( int jj=0; jj < nby; ++jj )
        bigmatrix[mati( ii, jj )] = h->GetBinContent( h->GetBin( ii+1, jj+1 ) );

    char xname[256], yname[256];
    strncpy( xname, name, 256 ); strcat( xname, "_x" );
    strncpy( yname, name, 256 ); strcat( yname, "_y" );

    fprintf( stream, "# name: %s\n", xname );
    fprintf( stream, "# type: matrix\n" );
    fprintf( stream, "# rows: 1\n" );
    fprintf( stream, "# columns; %d\n", nbx );
    for( int ii=0; ii < nbx; ++ii ) fprintf( stream, " %f", binx[ii] );
    fprintf( stream, "\n\n\n" );

    fprintf( stream, "# name: %s\n", yname );
    fprintf( stream, "# type: matrix\n" );
    fprintf( stream, "# rows: 1\n" );
    fprintf( stream, "# columns; %d\n", nby );
    for( int ii=0; ii < nby; ++ii ) fprintf( stream, " %f", biny[ii] );
    fprintf( stream, "\n\n\n" );

    fprintf( stream, "# name: %s\n", name );
    fprintf( stream, "# type: matrix\n" );
    fprintf( stream, "# rows: %d\n", nby );
    fprintf( stream, "# columns; %d\n", nbx );
    for( int jj=0; jj < nby; ++jj ){
        for( int ii=0; ii < nbx; ++ii ) fprintf( stream, " %f", bigmatrix[mati( ii, jj )] );
        fprintf( stream, "\n" );
    }
    fprintf( stream, "\n\n\n" );

    free( binx );
    free( biny );
    free( bigmatrix );
}
