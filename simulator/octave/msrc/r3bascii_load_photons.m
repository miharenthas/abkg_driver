%This tool loads photons from the files Dima generates
%
% [photons, evt_idx] = r3bascii_load_photons( fname )
% [photons, evt_idx, photon_events] = ...
%
%arguments:
% -- fname: the file name pointing to a .dat photon file
%returns:
% -- photons: an array of all the energies of those photons
% -- evt_idx: an array of indexes, referrint to 'photons', pointing to the beginning of each event.
% -- photon_event : the same, as cell, if requested.
function [photons, evt_idx, phev] = r3bascii_load_photons( fname )
    if ~ischar( fname ); error( "The file name is usually a string, don't you think?" ); end

    f = fopen( fname, 'r' );
    if f == -1; error( "Couldn't open the file" ); end

    photons = [];
    evt_idx = [1];
    if nargout == 3; phev = {}; end
    while 1
        ll = fgetl( f );
        if feof( f ); break; end
        ll = strsplit( ll );
        ll = cellfun( 'str2num', ll );
        photons = [photons,ll];
        evt_idx = [evt_idx,(evt_idx(end) + numel( ll ))];
        if nargout == 3; phev(end+1) = ll; end
    end

    fclose( f );
end
