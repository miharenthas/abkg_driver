%this tool will write photons
%
% r3bascii_write_photons( fname, photons )
% r3bascii_write_photons( fname, photons, evt_idx )
%
%arguments:
% -- fname: a string that is the file name
% -- photons: the photons either in a cell or in a huge array
% -- evt_idx: only if you used the huge array (otheriwse discarded), the event pointers.

function r3bascii_write_photons( fname, phot, evt_idx )
    if ~ischar( fname ); error( "The file name must be a string" ); end
    if nargin == 2 && ~iscell( phot ); error( "I need also the event indexes" ); end

    f = fopen( fname, 'w' );
    if f == -1; error( "Couldn't open the file." ); end

    if iscell( phot )
        for ii=1:numel( phot ); fdisp( f, num2str( phot{ii} ) ); end
    else
        for ii=1:numel( evt_idx )-1; fdisp( f, num2str( phot(evt_idx(ii):evt_idx(ii+1)-1) ) ); end
    end

    fflush( f );
    fclose( f );
end
